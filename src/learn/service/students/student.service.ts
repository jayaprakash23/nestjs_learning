/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StudentDto } from 'src/learn/dto/student.dto';
import { StudentEntity } from 'src/learn/entity/student.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StudentService {
    constructor(
        @InjectRepository(StudentEntity)
        private studentsRepository: Repository<StudentEntity>,
    ) {

    }

    async getAllStudents() {
        return await this.studentsRepository.find();
    }

    async createStudent(studentData: StudentDto) {
        const student = this.studentsRepository.create(studentData);
        await this.studentsRepository.save(studentData);
        return student;

    }

    async getByStudent(id: number) {
        return await this.studentsRepository.findOne({ where: { id: id } });
    }

    async getByStudentName(studentName: string) {
        return await this.studentsRepository.findOne({ where: { studentName: studentName } });
    }


    async updateStudent(id: number, studentData: Partial<StudentDto>) {
        await this.studentsRepository.update({ id }, studentData);
        return await this.studentsRepository.findOne({ id });
    }

    async deleteByStudent(id: number) {
        await this.studentsRepository.delete({ id });
        return { deleted: true };
    }

}
