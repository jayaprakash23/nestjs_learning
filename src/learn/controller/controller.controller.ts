/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';
import { StudentDto } from '../dto/student.dto';
import { StudentService } from '../service/students/student.service';

@Controller('student')
export class StudentController {
    constructor(
        private studentService: StudentService
    ) {

    }
    @Get()
    async getAllStudents() {
        return {
            statusCode: HttpStatus.OK,
            data: await this.studentService.getAllStudents(),
            message: 'Students All Get successfully',
        };
    }

    @Post()
    @ApiCreatedResponse({ description: 'Student creation' })
    @ApiBody({ type: StudentDto })
    async createStudent(@Body() data: StudentDto) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Student added successfully',
            data: await this.studentService.createStudent(data),
        };
    }

    @Get(':id')
    @ApiCreatedResponse({ description: 'Get by Student' })
    async getByStudent(@Param('id') id: number) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.studentService.getByStudent(id),
        };
    }
    @Get('/studentname/:studentName')
    @ApiCreatedResponse({ description: 'Get by Student Name' })
    async getByStudentName(@Param('studentName') studentName: string) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.studentService.getByStudentName(studentName),
        };
    }

    @Put(':id')
    async updateStudent(@Param('id') id: number, @Body() data: Partial<StudentDto>) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Student update successfully',
            data: await this.studentService.updateStudent(id, data),
        };
    }

    @Delete(':id')
    async deleteStudent(@Param('id') id: number) {
        await this.studentService.deleteByStudent(id);
        return {
            statusCode: HttpStatus.OK,
            message: 'Student deleted successfully',
        };
    }

}
