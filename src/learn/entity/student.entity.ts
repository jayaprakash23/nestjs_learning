import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('student')
export class StudentEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: true,
        unique: false,
        length: 500,
    })
    studentName: string;

    @Column()
    degree: string;

    @Column()
    college: string;

}
