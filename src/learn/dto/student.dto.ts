import { ApiProperty } from "@nestjs/swagger";

export class StudentDto {
    @ApiProperty({ type: Number, description: "id" })
    id: number;

    @ApiProperty({ type: String, description: "studentName" })
    studentName: string;

    @ApiProperty({ type: String, description: "degree" })
    degree: string;

    @ApiProperty({ type: String, description: "college" })
    college: string;

}

