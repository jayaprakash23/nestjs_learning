import { LoggerModule } from './logger/logger.module';
import { Module } from '@nestjs/common';
import configService from './config/config.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { StudentModule } from './learn/student.module';

@Module({
  imports: [
    LoggerModule,
    LoggerModule,
    StudentModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [configService],
      isGlobal: true,
    }),
    /** Configure TypeOrm asynchronously. */
    TypeOrmModule.forRootAsync({
      useFactory: async (
        configService: ConfigService,
      ): Promise<PostgresConnectionOptions> =>
        configService.get('postgresDatabase'),
      inject: [ConfigService],
    }),
  ],
})
export class AppModule { }
