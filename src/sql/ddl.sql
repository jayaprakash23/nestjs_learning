CREATE TABLE IF NOT EXISTS public.student
(
    id integer NOT NULL DEFAULT nextval('student_id_seq'::regclass),
    degree character varying COLLATE pg_catalog."default",
    college character varying COLLATE pg_catalog."default",
    "studentName" character varying COLLATE pg_catalog."default",
    CONSTRAINT new_student PRIMARY KEY (id)
)