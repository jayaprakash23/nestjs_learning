import { registerAs } from '@nestjs/config';
require("dotenv").config();
export default registerAs('postgresDatabase', () => ({
  type: 'postgres',
  host: process.env.DB_HOST,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  logging: process.env.DB_LOGGING === 'true',
  synchronize: process.env.DB_SYNC === 'true',
  entities: ['dist/**/**.entity{.ts,.js}'],
  port: parseInt(process.env.DB_PORT, 10),
}));
